#!/bin/bash

ARTIFACTS_DIR="./help-scripts/artifacts"
if [ ! -d "$ARTIFACTS_DIR" ]; then
    mkdir ./help-scripts/artifacts
fi

FILE_NAME="structure.png"
CUSTOM_NAME="$1"
if [ ! -z "$CUSTOM_NAME" ]; then
    FILE_NAME="$CUSTOM_NAME"
fi

python manage.py graph_models -a -t django2018 -g -o "help-scripts/artifacts/$FILE_NAME" 
echo "Generated and put project structure to the 'help-scripts/artifacts' folder"
