def coded_assign_operation_factory(left_operand, right_operand):
    return {
        'type': 'assign',
        'parameter': {
            'left': left_operand,
            'right': right_operand
        }
    }


def coded_variable_operation_factory(name):
    return {
        'type': 'variable',
        'parameter': {
            'name': name
        }
    }


def coded_condition_operation_factory(condition):
    return {
        'type': 'condition',
        'parameter': {
            'param1': condition
        }
    }


def coded_end_condition_operation_factory():
    return {
        'type': 'end-condition',
        'parameter': {}
    }


def coded_for_loop_operation_factory(index, start, end, step):
    return {
        'type': 'for-loop',
        'parameter': {
            'index': index,
            'start': start,
            'end': end,
            'step': step
        }
    }


def coded_end_for_loop_operation_factory():
    return {
        'type': 'end-for-loop',
        'parameter': {}
    }
