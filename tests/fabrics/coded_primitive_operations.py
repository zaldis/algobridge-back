import pytest


@pytest.fixture
def coded_positive_number_operation():
    return coded_number_operation_factory(10)


@pytest.fixture
def coded_negative_number_operation():
    return coded_number_operation_factory(-10)


def coded_number_operation_factory(value):
    return {
        'type': 'number',
        'parameter': {
            'val': value
        }
    }
