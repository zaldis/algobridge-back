def coded_sum_operation_factory(left_operand, right_operand):
    return {
        'type': 'sum',
        'parameter': {
            'left': left_operand,
            'right': right_operand
        }
    }


def coded_subtraction_operation_factory(left_operand, right_operand):
    return {
        'type': 'subtraction',
        'parameter': {
            'left': left_operand,
            'right': right_operand
        }
    }


def coded_multiplication_operation_factory(left_operand, right_operand):
    return {
        'type': 'multiplication',
        'parameter': {
            'left': left_operand,
            'right': right_operand
        }
    }


def coded_division_operation_factory(left_operand, right_operand):
    return {
        'type': 'division',
        'parameter': {
            'left': left_operand,
            'right': right_operand
        }
    }
