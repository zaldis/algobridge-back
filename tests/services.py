from rest_framework.authtoken.models import Token


def apply_credentials_for_api_client(api_client, user):
    api_client.force_authenticate(user=user)
    token = Token.objects.get(user=user)
    api_client.credentials(Authorization=f'Token {token.key}')
