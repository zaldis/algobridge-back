import pytest

from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token


@pytest.fixture
def guest_user():
    """
        User without registration token
    """
    return guest_user_factory('guest', 'guest@mail.com', '123')


@pytest.fixture
def standard_user():
    """
        User with registration token but without admin staff
    """
    return standard_user_factory('standard', 'standard@mail.com', '123')


@pytest.fixture
def admin_user(standard_user):
    """
        User with admin staff and registration token
    """
    standard_user.username = 'admin'
    standard_user.is_superuser = True
    standard_user.save()
    return standard_user


def guest_user_factory(name, email, password):
    return User.objects.create_user(
        username=name,
        email=email,
        password=password
    )

def standard_user_factory(name, email, password):
    user = guest_user_factory(name, email, password)
    Token.objects.create(user=user)
    return user
