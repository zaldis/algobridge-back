import pytest

from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from tests.fixtures.users import admin_user, standard_user, guest_user


class TestUsersCreation(object):

    @pytest.mark.django_db
    def test_admin_user(self, admin_user):
        assert User.objects.count() == 1
        assert admin_user.is_superuser == True
        assert Token.objects.filter(user=admin_user).count() == 1

    @pytest.mark.django_db
    def test_standard_user(self, standard_user):
        assert User.objects.count() == 1
        assert standard_user.is_superuser == False
        assert Token.objects.filter(user=standard_user).count() == 1
