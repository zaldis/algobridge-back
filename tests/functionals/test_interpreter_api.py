import json

import pytest

from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.request import Request
from rest_framework.test import APIClient

from tests.fixtures.users import standard_user, standard_user_factory
from tests.fabrics.coded_arithmetic_operations import (
    coded_division_operation_factory,
    coded_multiplication_operation_factory,
    coded_subtraction_operation_factory,
    coded_sum_operation_factory,
)
from tests.fabrics.coded_primitive_operations import (
    coded_number_operation_factory,
)
from tests.fabrics.coded_base_operations import (
    coded_assign_operation_factory,
    coded_condition_operation_factory,
    coded_end_condition_operation_factory,
    coded_end_for_loop_operation_factory,
    coded_for_loop_operation_factory,
    coded_variable_operation_factory,
)
from tests.services import apply_credentials_for_api_client


@pytest.mark.django_db
def test_unauthorized_interpreter_execution(client, standard_user):
    coded_algo = '[]'
    url = reverse('interpreter-run-implementation')
    response = client.post(url, user=standard_user, args={'operations': coded_algo})
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


class TestInterpreterAPI:

    @pytest.mark.django_db
    def setup_method(self, method):
        self.api_client: APIClient = APIClient()
        standard_user = standard_user_factory('Antony', 'antony@mail.com', '123')
        apply_credentials_for_api_client(self.api_client, standard_user)

    @pytest.mark.django_db
    def teardown_method(self, method):
        User.objects.all().delete()
        Token.objects.all().delete()

    @pytest.mark.django_db
    def test_running_sum_operation(self):
        coded_result = coded_variable_operation_factory('result')
        coded_number_5 = coded_number_operation_factory(5)
        coded_number_13 = coded_number_operation_factory(13)
        coded_sum = coded_sum_operation_factory(coded_number_5, coded_number_13)
        coded_assign = coded_assign_operation_factory(coded_result, coded_sum)

        coded_algo = json.dumps([coded_assign])
        response = self.__request_to_interpreter(coded_algo)

        assert response.status_code == status.HTTP_200_OK
        assert response.json() == [{'name': 'result', 'type': 'number', 'value': 18}]

    @pytest.mark.django_db
    def test_running_subtraction_operation(self):
        coded_result = coded_variable_operation_factory('result')
        coded_number_5 = coded_number_operation_factory(5)
        coded_number_13 = coded_number_operation_factory(13)
        coded_subtraction = coded_subtraction_operation_factory(coded_number_5, coded_number_13)
        coded_assign = coded_assign_operation_factory(coded_result, coded_subtraction)

        coded_algo = json.dumps([coded_assign])
        response = self.__request_to_interpreter(coded_algo)

        assert response.status_code == status.HTTP_200_OK
        assert response.json() == [{'name': 'result', 'type': 'number', 'value': -8}]

    @pytest.mark.django_db
    def test_running_multiplication_operation(self):
        coded_result = coded_variable_operation_factory('result')
        coded_number_5 = coded_number_operation_factory(5)
        coded_number_7 = coded_number_operation_factory(7)
        coded_multiplication = coded_multiplication_operation_factory(coded_number_5, coded_number_7)
        coded_assign = coded_assign_operation_factory(coded_result, coded_multiplication)

        coded_algo = json.dumps([coded_assign])
        response = self.__request_to_interpreter(coded_algo)

        assert response.status_code == status.HTTP_200_OK
        assert response.json() == [{'name': 'result', 'type': 'number', 'value': 35}]

    @pytest.mark.django_db
    def test_running_division_operation(self):
        coded_result = coded_variable_operation_factory('result')
        coded_number_25 = coded_number_operation_factory(25)
        coded_number_5 = coded_number_operation_factory(5)
        coded_division = coded_division_operation_factory(coded_number_25, coded_number_5)
        coded_assign = coded_assign_operation_factory(coded_result, coded_division)

        coded_algo = json.dumps([coded_assign])
        response = self.__request_to_interpreter(coded_algo)

        assert response.status_code == status.HTTP_200_OK
        assert response.json() == [{'name': 'result', 'type': 'number', 'value': 5}]

    @pytest.mark.django_db
    def test_running_true_condition_operation(self):
        coded_result = coded_variable_operation_factory('result')
        coded_number_25 = coded_number_operation_factory(25)
        coded_number_5 = coded_number_operation_factory(5)

        coded_assign = coded_assign_operation_factory(coded_result, coded_number_25)
        coded_true_condition = coded_condition_operation_factory(coded_number_5)
        coded_condition_body = coded_assign_operation_factory(coded_result, coded_number_5)
        coded_end_condition = coded_end_condition_operation_factory()

        coded_algo = json.dumps([
            coded_assign,
            coded_true_condition,
            coded_condition_body,
            coded_end_condition
        ])
        response = self.__request_to_interpreter(coded_algo)

        assert response.status_code == status.HTTP_200_OK
        assert response.json() == [{'name': 'result', 'type': 'number', 'value': 5}]

    @pytest.mark.django_db
    def test_running_false_condition_operation(self):
        coded_result = coded_variable_operation_factory('result')
        coded_number_25 = coded_number_operation_factory(25)
        coded_number_5 = coded_number_operation_factory(5)
        coded_number_0 = coded_number_operation_factory(0)

        coded_assign = coded_assign_operation_factory(coded_result, coded_number_25)
        coded_false_condition = coded_condition_operation_factory(coded_number_0)
        coded_condition_body = coded_assign_operation_factory(coded_result, coded_number_5)
        coded_end_condition = coded_end_condition_operation_factory()

        coded_algo = json.dumps([
            coded_assign,
            coded_false_condition,
            coded_condition_body,
            coded_end_condition
        ])
        response = self.__request_to_interpreter(coded_algo)

        assert response.status_code == status.HTTP_200_OK
        assert response.json() == [{'name': 'result', 'type': 'number', 'value': 25}]

    @pytest.mark.django_db
    def test_running_for_loop_operation(self):
        coded_index = coded_variable_operation_factory('index')
        coded_result = coded_variable_operation_factory('result')
        coded_delta = coded_number_operation_factory(1)
        coded_start = coded_number_operation_factory(0)
        coded_end = coded_number_operation_factory(5)

        coded_assign_result = coded_assign_operation_factory(coded_result, coded_start)
        coded_for_loop = coded_for_loop_operation_factory(coded_index, coded_start, coded_end, coded_delta)
        coded_increased_result = coded_sum_operation_factory(coded_result, coded_index)
        coded_assign_increased_result = coded_assign_operation_factory(coded_result, coded_increased_result)
        coded_end_for_loop = coded_end_for_loop_operation_factory()

        coded_algo = json.dumps([
            coded_assign_result,
            coded_for_loop,
            coded_assign_increased_result,
            coded_end_for_loop
        ])
        response = self.__request_to_interpreter(coded_algo)

        assert response.status_code == status.HTTP_200_OK
        assert response.json() == [
                                    {'name': 'result', 'type': 'number', 'value': 15},
                                    {'name': 'index', 'type': 'number', 'value': 6}
                                  ]

    @pytest.mark.django_db
    def test_running_operation_with_invalid_type(self):
        invalid_operation = {'type': 'invalid-operation-type'}
        coded_algo = json.dumps([invalid_operation])
        response = self.__request_to_interpreter(coded_algo)
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def __request_to_interpreter(self, coded_algo, execution_type='simple') -> Request:
        url = reverse('interpreter-run-implementation')
        response = self.api_client.post(
            url,
            {'type': execution_type, 'operations': coded_algo},
            format='json'
        )
        return response
