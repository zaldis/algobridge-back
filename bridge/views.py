import json
import hashlib
from typing import Any, List, NamedTuple, Text

from django.conf import settings
from django.contrib.auth.models import User
from django.core.cache import cache
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from rest_framework import generics, status, viewsets
from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import action, api_view

from .models import Algorithm, OperationType
from .interpreter.interpreter import Interpreter
from .interpreter.data_structures.variable import Variable
from .serializers import (
    AlgorithmSerializer, OperationTypeSerializer, RegisterSerializer, OperationSerializer,
    VariableSerializer, UserSerializer
)
from .services import is_email_exist, is_user_exist


CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


@api_view(['GET'])
def user_info(request):
    serializer = UserSerializer(request.user)
    return Response({'user': serializer.data}, status=status.HTTP_200_OK)


class RegisterAPI(generics.CreateAPIView):
    serializer_class = RegisterSerializer

    def post(self, request):
        serializer = RegisterSerializer(data=request.data)
        if serializer.is_valid():
            name = serializer.validated_data['username']
            email = serializer.validated_data['email']
            password = serializer.validated_data['password']

            if is_user_exist(name):
                return Response(
                    {"error": f"User with '{name}' username already exist"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            if is_email_exist(email):
                return Response(
                    {"error": f"The email '{email}' was already used"},
                    status=status.HTTP_400_BAD_REQUEST,
                )

            created_user = User.objects.create_user(name, email, password)
            created_token, _ = Token.objects.get_or_create(user=created_user)

            return Response(
                {"token_key": created_token.key},
                status=status.HTTP_201_CREATED,
            )
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)


class AlgorithmAPI(viewsets.ModelViewSet):
    serializer_class = AlgorithmSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Algorithm.objects.filter(author=self.request.user.id)

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            author = request.user
            title = serializer.validated_data['title']
            description = serializer.validated_data['description']
            implementation = serializer.validated_data['implementation']

            created_algo = Algorithm.objects.create(
                author=author, title=title, description=description,
                implementation=implementation
            )
            return Response({'message': 'Successfully created', 'algo_id': created_algo.id},
                    status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class InterpreterAPI(viewsets.GenericViewSet):
    queryset = []
    serializer_class = OperationSerializer
    permission_classes = [IsAuthenticated]

    @action(detail=False, methods=['POST'])
    def run_implementation(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            implementation_type = serializer.validated_data['type']
            IS_VISUAL = implementation_type == 'visual'
            OPERATION_KEY = serializer.validated_data['operations'] + ('1' if IS_VISUAL else '0')
            OPERATION_KEY_HASH = hashlib.md5(OPERATION_KEY.encode()).hexdigest()
            if OPERATION_KEY_HASH in cache:
                return Response(cache.get(OPERATION_KEY_HASH))

            interpreter_result = self.__run_interpreter(serializer.validated_data)
            if interpreter_result.has_error:
                response_data = {'error': str(interpreter_result.error)}
                cache.set(OPERATION_KEY_HASH, response_data, CACHE_TTL)
                return Response(response_data, status=status.HTTP_400_BAD_REQUEST)

            var_serializer = VariableSerializer(interpreter_result.out_variables, many=True)
            if IS_VISUAL:
                response_data = {
                    'state': var_serializer.data,
                    'visual_operations': interpreter_result.visual_operations
                }
                cache.set(OPERATION_KEY_HASH, response_data, CACHE_TTL)
                return Response(response_data, status=status.HTTP_200_OK)
            cache.set(OPERATION_KEY_HASH, var_serializer.data, CACHE_TTL)
            return Response(var_serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    class InterpreterInvokingResult(NamedTuple):
        has_error: bool = False
        error: Text = ''
        out_variables: List[Variable] = []
        visual_operations: List[Any] = []

    def __run_interpreter(self, validated_data) -> InterpreterInvokingResult:
        try:
            interpreter = Interpreter(
                json.loads(validated_data.get('operations', '[]'))
            )
            interpreter.implement()
        except ValueError as err:
            return self.InterpreterInvokingResult(has_error=True, error=str(err))

        created_vars = []
        for var in interpreter.state.saved_variables.values():
            created_vars.append(var)
        return self.InterpreterInvokingResult(out_variables=created_vars,
                                              visual_operations=interpreter.visual_operations)


class OperationTypeAPIViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = OperationType.objects.filter(enabled=True)
    serializer_class = OperationTypeSerializer
