from django.contrib.auth.models import User
from rest_framework import serializers

from .models import Algorithm, OperationType


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'email')


class RegisterSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=50)
    email = serializers.EmailField()
    password = serializers.CharField()


class AlgorithmSerializer(serializers.ModelSerializer):
    class Meta:
        model = Algorithm
        read_only_fields = ['author']
        exclude = ('author', )


class OperationSerializer(serializers.Serializer):
    operations = serializers.JSONField()
    type = serializers.JSONField()


class OperationTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = OperationType
        exclude = ('enabled',)
        depth = 1

    parameters = serializers.SerializerMethodField()

    def get_parameters(self, operaion_type):
        return map(lambda item: item.strip(), operaion_type.parameters.split(','))


class VariableSerializer(serializers.Serializer):
    name = serializers.CharField()
    type = serializers.SerializerMethodField('variable_type')
    value = serializers.SerializerMethodField('variable_value')

    def variable_value(self, variable):
        return variable.saved_operation.value

    def variable_type(self, variable):
        return variable.saved_operation.type.value
