import os

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        User = get_user_model()

        super_name = os.environ.get('super_name', 'admin')
        super_pass = os.environ.get('super_pass', 'admin')
        super_email = os.environ.get('super_emial', 'admin@gmail.com')

        if not User.objects.filter(username=super_name).exists():
            User.objects.create_superuser(super_name, super_email, super_pass)
