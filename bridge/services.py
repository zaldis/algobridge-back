from django.contrib.auth.models import User


def is_user_exist(username):
    return User.objects.filter(username=username).count() > 0


def is_email_exist(email):
    return User.objects.filter(email=email).count() > 0
