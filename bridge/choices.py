OPERATION_PARAM_TOKEN = 'operation_param_token'
OPERATION_BODY_TOKEN = 'operation_body_token'

OPERATION_TOKEN_TYPE = (
    (OPERATION_PARAM_TOKEN, 'Parameter token'),
    (OPERATION_BODY_TOKEN, 'Body token')
)


ASSIGN_OPERATION = 'assign'
LARGER_OPERATION = 'larger'
LARGER_EQUAL_OPERATION = 'larger-equal'
LESS_OPERATION = 'less'
LESS_EQUAL_OPERATION = 'less-equal'
EQUAL_OPERATION = 'equal'
AND_LOGIC_OPERATION = 'and-logic'
OR_LOGIC_OPERATION = 'or-logic'
NUMBER_OPERATION = 'number'
VARIABLE_OPERATION = 'variable'
FOR_LOOP_OPERATION = 'for-loop'
END_FOR_LOOP_OPERATION = 'end-for-loop'
ARRAY_OPERATION = 'array'
SUM_OPERATION = 'sum'
SUBTRACTION_OPERATION = 'subtraction'
MULTIPLICATION_OPERATION = 'multiplication'
DIVISION_OPERATION = 'division'
CONDITION_OPERATION = 'condition'
END_CONDITION_OPERATION = 'end-condition'
GET_ITEM_OPERATION = 'get-item'
SET_ITEM_OPERATION = 'set-item'

OPERATION_TYPES = (
    (ASSIGN_OPERATION, 'Assign'),
    (LARGER_OPERATION, 'Larger'),
    (LARGER_EQUAL_OPERATION, 'Larger or equal'),
    (LESS_OPERATION, 'Less'),
    (LESS_EQUAL_OPERATION, 'Less or equal'),
    (EQUAL_OPERATION, 'Equal'),
    (AND_LOGIC_OPERATION, 'Logic &'),
    (OR_LOGIC_OPERATION, 'Logic |'),
    (NUMBER_OPERATION, 'Number'),
    (VARIABLE_OPERATION, 'Variable'),
    (FOR_LOOP_OPERATION, 'For loop'),
    (END_FOR_LOOP_OPERATION, 'End for loop'),
    (ARRAY_OPERATION, 'Array'),
    (SUM_OPERATION, 'Sum'),
    (SUBTRACTION_OPERATION, 'Subtraction'),
    (MULTIPLICATION_OPERATION, 'Multiplication'),
    (DIVISION_OPERATION, 'Division'),
    (CONDITION_OPERATION, 'Condition'),
    (END_CONDITION_OPERATION, 'End condition'),
    (GET_ITEM_OPERATION, 'Get item'),
    (SET_ITEM_OPERATION, 'Set item'),
)
