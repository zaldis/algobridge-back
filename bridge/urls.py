from django.urls import path
from rest_framework import routers
from rest_framework.authtoken import views

from .views import (
    AlgorithmAPI, RegisterAPI, InterpreterAPI, user_info, OperationTypeAPIViewSet
)

router = routers.SimpleRouter()
router.register('algos', AlgorithmAPI, basename='algorithms')
router.register('interpreter', InterpreterAPI, basename='interpreter')
router.register('operation-type', OperationTypeAPIViewSet, basename='operation-type')

urlpatterns = [
    path('api-token-auth/', views.obtain_auth_token, name='auth'),
    path('register/', RegisterAPI.as_view(), name='registration'),
    path('user-info/', user_info, name="user-info"),
]

urlpatterns += router.urls
