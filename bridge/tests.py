from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from .interpreter import state


class RegistrationTestCase(TestCase):

    client = APIClient()

    register_content_a = {
        "username": "Anton",
        "email": "anton@gmail.com",
        "password": "strongly password"
    }

    register_content_b = {
        'username': 'Anton',
        'email': 'darton@gmail.com',
        'password': 'pass tone'
    }

    register_content_c = {
        'username': 'Ivan',
        'email': 'anton@gmail.com',
        'password': 'ivan password'
    }

    def test_registration(self):
        request = self.client.post(
            reverse('registration'),
            data=self.register_content_a,
        )
        self.assertContains(request, 'token_key',
                            status_code=status.HTTP_201_CREATED)
        token_key = request.data.get('token_key')

        auth_content = {
            'username': self.register_content_a['username'],
            'email': self.register_content_a['email'],
            'password': self.register_content_a['password']
        }
        auth_request = self.client.post(
            reverse('auth'),
            data=auth_content,
        )
        self.assertContains(auth_request, 'token')
        self.assertEqual(token_key, auth_request.data['token'])

    def test_registration_double_username(self):
        request = self.client.post(
            reverse('registration'),
            data=self.register_content_a,
        )
        self.assertContains(
            request, 'token_key', status_code=status.HTTP_201_CREATED)

        request = self.client.post(
            reverse('registration'),
            data=self.register_content_b,
        )
        self.assertContains(
            request, 'error', status_code=status.HTTP_400_BAD_REQUEST)

    def test_registration_double_email(self):
        request = self.client.post(
            reverse('registration'),
            data=self.register_content_a,
        )
        self.assertContains(
            request, 'token_key', status_code=status.HTTP_201_CREATED)

        request = self.client.post(
            reverse('registration'),
            data=self.register_content_c,
        )
        self.assertContains(
            request, 'error', status_code=status.HTTP_400_BAD_REQUEST)


class AlgorithmAPITestCase(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.user_pavel = User.objects.create_user('pavel', 'pavel@gmail.com', 'password')
        self.user_anton = User.objects.create_user('anton', 'anton@gmail.com', 'password')

        self.pavel_token, _ = Token.objects.get_or_create(user=self.user_pavel)
        self.anton_token, _ = Token.objects.get_or_create(user=self.user_anton)

        self.algo_a = {
            'title': 'AlgoA',
            'description': 'some description a',
            'implementation': '[]'
        }
        self.algo_b = {
            'title': 'AlgoB',
            'description': 'some description b',
            'implementation': '[]'
        }

    def test_algorithm_creation(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.pavel_token}')
        request = self.client.get(
            reverse('algorithms-list')
        )
        self.assertListEqual(request.data, [])

        request = self.client.post(
            reverse('algorithms-list'), data=self.algo_a
        )

        request = self.client.get(
            reverse('algorithms-list')
        )
        self.assertEqual(len(request.data), 1)
        self.assertListEqual(
            list(request.data[0].keys()),
            ['id', 'title', 'description', 'implementation']
        )

    def test_algorithm_update(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.pavel_token}')
        self.client.post(
            reverse('algorithms-list'), data=self.algo_a
        )

        request_initial = self.client.get(
            reverse('algorithms-list')
        )
        self.assertEqual(
            request_initial.data[0]['title'], self.algo_a['title']
        )

        self.client.put(
            reverse('algorithms-detail', args=[request_initial.data[0]['id']]),
            data=self.algo_b
        )

        request_updated = self.client.get(
            reverse('algorithms-list')
        )
        self.assertEqual(len(request_updated.data), 1)
        self.assertEqual(
            request_updated.data[0]['title'], self.algo_b['title']
        )

    def test_algorithm_delete(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.pavel_token}')
        self.client.post(
            reverse('algorithms-list'), data=self.algo_a
        )

        request_initial = self.client.get(
            reverse('algorithms-list')
        )
        self.assertEqual(
            request_initial.data[0]['title'], self.algo_a['title']
        )

        self.client.delete(
            reverse('algorithms-detail', args=[request_initial.data[0]['id']])
        )

        request_deleted = self.client.get(
            reverse('algorithms-list')
        )
        self.assertListEqual(request_deleted.data, [])
