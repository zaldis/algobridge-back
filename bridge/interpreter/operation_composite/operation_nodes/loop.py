from bridge.interpreter.interfaces.state import State
from bridge.interpreter.operation_composite.operation_nodes.assign import AssignNodeOperation
from bridge.interpreter.operation_composite.operation_nodes.number import NumberNodeOperation
from bridge.interpreter.operation_composite.operation_nodes.empty import EmptyNodeOperation
from bridge.interpreter.constants import ErrorMessages, Limits
from typing import Dict

from ..choices import OperationNodeTypes
from ..interfaces import OperationNode
from ..services import is_assigned_variable_node


class LoopNodeOperation(OperationNode):

    def __init__(self, index: OperationNode, start: OperationNode, end: OperationNode, step: OperationNode):
        self.__index = index
        self.__start = start
        self.__end = end
        self.__step = step

    @classmethod
    def create_from_code(cls, operation_data: Dict) -> OperationNode:
        from ..operation_converter import convert_to_operation

        index = convert_to_operation(operation_data['parameter'].get('index'))
        start = convert_to_operation(operation_data['parameter'].get('start'))
        end = convert_to_operation(operation_data['parameter'].get('end'))
        step = convert_to_operation(operation_data['parameter'].get('step'))
        return cls(index, start, end, step)

    @property
    def index(self):
        return self.__index

    @property
    def start(self):
        return self.__start

    @property
    def end(self):
        return self.__end

    @property
    def step(self):
        return self.__step

    @property
    def type(self):
        return OperationNodeTypes.LOOP_NODE

    @property
    def value(self):
        raise NotImplementedError()

    def primitive_value(self, interpreter_state):
        return EmptyNodeOperation()

    def run(self, interpreter_state: State):
        self.index.run(interpreter_state)
        self.start.run(interpreter_state)
        self.end.run(interpreter_state)
        self.step.run(interpreter_state)

        self.__validate_operation(interpreter_state)

        index = self.index.primitive_value(interpreter_state).value or 0
        start = self.start.primitive_value(interpreter_state).value
        end = self.end.primitive_value(interpreter_state).value
        step = self.step.primitive_value(interpreter_state).value

        ROW_NUMBER = interpreter_state.cursor_row_number
        loop_iterations = interpreter_state.get_loop_iterations(ROW_NUMBER)
        next_index = start
        if loop_iterations > Limits.MAX_LOOP_COUNT:
            raise ValueError(ErrorMessages.INFINITE_FOR_LOOP.format(ROW_NUMBER, Limits.MAX_LOOP_COUNT))
        if loop_iterations > 0:
            next_index = index + step
        interpreter_state.up_loop_iterations(ROW_NUMBER)

        new_index_value_node = NumberNodeOperation(next_index)
        assign_node = AssignNodeOperation(self.index, new_index_value_node)
        assign_node.run(interpreter_state)

        if next_index > end:
            end_loop_block = interpreter_state.get_block_end(ROW_NUMBER)
            interpreter_state.reset_loop_iterations(ROW_NUMBER)
            interpreter_state.move_cursor_to(end_loop_block)

    def __validate_operation(self, interpreter_state: State):
        ROW_NUMBER = interpreter_state.cursor_row_number
        if not (self.index and self.start and self.end and self.step):
            raise ValueError(ErrorMessages.INVALID_FOR_LOOP_PARAMETERS.format(ROW_NUMBER))

        end_condition = interpreter_state.get_block_end(ROW_NUMBER)
        if not end_condition:
            raise ValueError(
                ErrorMessages.FOR_LOOP_WITHOUT_END.format(ROW_NUMBER)
            )

        if not is_assigned_variable_node(self.start, interpreter_state):
            raise ValueError(
                ErrorMessages.NON_INITIALIZE_VARIABLE.format(
                    ROW_NUMBER, self.start.value
                )
            )
        if not is_assigned_variable_node(self.end, interpreter_state):
            raise ValueError(
                ErrorMessages.NON_INITIALIZE_VARIABLE.format(
                    ROW_NUMBER, self.end.value
                )
            )
        if not is_assigned_variable_node(self.step, interpreter_state):
            raise ValueError(
                ErrorMessages.NON_INITIALIZE_VARIABLE.format(
                    ROW_NUMBER, self.step.value
                )
            )
        self.__validate_parameter_types(interpreter_state)

    def __validate_parameter_types(self, interpreter_state: State):
        is_invalid_types = False

        if self.start.primitive_value(interpreter_state).type != OperationNodeTypes.NUMBER_NODE:
            is_invalid_types = True

        if self.end.primitive_value(interpreter_state).type != OperationNodeTypes.NUMBER_NODE:
            is_invalid_types = True

        if self.step.primitive_value(interpreter_state).type != OperationNodeTypes.NUMBER_NODE:
            is_invalid_types = True

        if is_invalid_types:
            ROW_NUMBER = interpreter_state.cursor_row_number
            raise ValueError(
                ErrorMessages.INVALID_FOR_LOOP_PARAMETERS.format(ROW_NUMBER)
            )
