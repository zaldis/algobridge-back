from bridge.interpreter.interfaces.state import State
from bridge.interpreter.operation_composite.operation_nodes.empty import EmptyNodeOperation
from bridge.interpreter.compiler import VisualOperationCompiler
from bridge.interpreter.constants import ErrorMessages

from ..choices import OperationNodeTypes
from ..interfaces import OperationNode
from ..services import is_assigned_variable_node, is_primitive_node


class AssignNodeOperation(OperationNode):

    def __init__(self, left_node: OperationNode, right_node: OperationNode):
        self.__left_node = left_node
        self.__right_node = right_node

    @classmethod
    def create_from_code(cls, operation_data):
        from ..operation_converter import convert_to_operation

        left_data = operation_data['parameter'].get('left')
        right_data = operation_data['parameter'].get('right')
        left_node = left_data and convert_to_operation(left_data)
        right_node = right_data and convert_to_operation(right_data)

        return cls(left_node, right_node)

    @property
    def left_node(self):
        return self.__left_node

    @property
    def right_node(self):
        return self.__right_node

    @property
    def type(self):
        return OperationNodeTypes.ASSIGN_NODE

    @property
    def value(self):
        raise NotImplementedError()

    def primitive_value(self, interpreter_state):
        return EmptyNodeOperation()

    def run(self, interpreter_state):
        self.left_node.run(interpreter_state)
        self.right_node.run(interpreter_state)

        self.__validate_operation(interpreter_state)

        if self.right_node.type == OperationNodeTypes.VARIABLE_NODE:
            self.__display_visual_variable_assign(interpreter_state)
        else:
            self.__display_visual_primitive_assign(interpreter_state)

        interpreter_state[self.left_node.value] = self.right_node.primitive_value(interpreter_state)

    def __display_visual_variable_assign(self, interpreter_state: State):
        interpreter_state.add_visual_command(
            VisualOperationCompiler.compile_from_variable_assign_operation(
                interpreter_state.cursor_row_number,
                str(self.left_node.value),
                str(self.left_node.primitive_value(interpreter_state).value),
                str(self.right_node.value),
                str(self.right_node.primitive_value(interpreter_state).value),
            )
        )

    def __display_visual_primitive_assign(self, interpreter_state: State):
        interpreter_state.add_visual_command(
            VisualOperationCompiler.compile_simple_assign_operation(
                interpreter_state.cursor_row_number,
                str(self.left_node.value),
                str(self.left_node.primitive_value(interpreter_state).value),
                str(self.right_node.primitive_value(interpreter_state).value)
            )
        )

    def __validate_operation(self, interpreter_state: State):
        ROW_NUMBER = interpreter_state.cursor_row_number
        if not self.left_node or not self.right_node:
            raise ValueError(ErrorMessages.INVALID_ASSIGN.format(ROW_NUMBER))

        if not is_assigned_variable_node(self.right_node, interpreter_state):
            raise ValueError(
                ErrorMessages.NON_INITIALIZE_VARIABLE.format(
                    ROW_NUMBER, self.right_node.value
                )
            )
        self.__validate_parameter_types(interpreter_state)

    def __validate_parameter_types(self, interpreter_state):
        is_invalid_types = False

        if self.left_node.type != OperationNodeTypes.VARIABLE_NODE:
            is_invalid_types = True

        if not is_primitive_node(self.right_node.primitive_value(interpreter_state)):
            is_invalid_types = True

        if is_invalid_types:
            ROW_NUMBER = interpreter_state.cursor_row_number
            raise ValueError(
                ErrorMessages.INVALID_ASSIGN_PARAMETERS.format(ROW_NUMBER)
            )
