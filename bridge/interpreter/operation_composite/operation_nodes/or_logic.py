from bridge.interpreter.interfaces.state import State
from bridge.interpreter.constants import ErrorMessages

from .mixins.binary_node import BinaryOperationNodeMixin
from ..choices import OperationNodeTypes
from ..interfaces import OperationErrorTemplates, OperationNode


class OrLogicNodeOperation(BinaryOperationNodeMixin, OperationNode):

    @property
    def type(self):
        return OperationNodeTypes.OR_LOGIC_NODE

    @property
    def value(self):
        raise NotImplementedError()

    def primitive_value(self, interpreter_state):
        return self.result

    def run(self, interpreter_state: State):
        error_templates = OperationErrorTemplates(
            invalid_operation=ErrorMessages.INVALID_LOGIC_OPERATION,
            invalid_parameter=ErrorMessages.INVALID_LOGIC_PARAMETERS,
        )
        self._run_binary_operation(interpreter_state,
                                   lambda left_value, right_value: left_value or right_value,
                                   error_templates)
