from typing import Any, Callable

from bridge.interpreter.interfaces.state import State
from bridge.interpreter.interfaces.cursor_manager import CursorManager
from bridge.interpreter.operation_composite.services import is_assigned_variable_node, is_number_node
from bridge.interpreter.operation_composite.interfaces import OperationErrorTemplates, OperationNode
from ..number import NumberNodeOperation
from ..empty import EmptyNodeOperation


class BinaryOperationNodeMixin(object):

    def __init__(self, left_node: OperationNode, right_node: OperationNode):
        self.__left_node = left_node
        self.__right_node = right_node
        self.__result = EmptyNodeOperation()

    @classmethod
    def create_from_code(cls, operation_data):
        from bridge.interpreter.operation_composite.operation_converter import convert_to_operation

        left_data = operation_data['parameter'].get('left')
        right_data = operation_data['parameter'].get('right')
        left_node = left_data and convert_to_operation(left_data)
        right_node = right_data and convert_to_operation(right_data)
        return cls(left_node, right_node)

    @property
    def left_node(self):
        return self.__left_node

    @property
    def right_node(self):
        return self.__right_node

    @property
    def result(self):
        return self.__result

    def _run_binary_operation(self,
                              interpreter_state: State,
                              target_function: Callable[[Any, Any], Any],
                              error_templates: OperationErrorTemplates):
        self.left_node.run(interpreter_state)
        self.right_node.run(interpreter_state)

        self.__validate_operation(interpreter_state, error_templates)
        self.__validate_parameter_types(interpreter_state, error_templates)

        left_value = self.left_node.primitive_value(interpreter_state).value
        right_value = self.right_node.primitive_value(interpreter_state).value
        primitive_result = target_function(left_value, right_value)
        self.__result = NumberNodeOperation(primitive_result)

    def __validate_operation(self,
                             interpreter_state: State,
                             error_templates: OperationErrorTemplates):
        ROW_NUMBER = interpreter_state.cursor_row_number
        if not self.left_node or not self.right_node:
            raise ValueError(
                error_templates.invalid_operation.format(ROW_NUMBER)
            )

        if not is_assigned_variable_node(self.left_node, interpreter_state):
            raise ValueError(
                error_templates.non_initialize.format(
                    ROW_NUMBER, self.left_node.value
                )
            )

        if not is_assigned_variable_node(self.right_node, interpreter_state):
            raise ValueError(
                error_templates.non_initialize.format(
                    ROW_NUMBER, self.right_node.value
                )
            )

    def __validate_parameter_types(self,
                                   interpreter_state,
                                   error_templates: OperationErrorTemplates):
        is_invalid_types = False

        if not is_number_node(self.left_node.primitive_value(interpreter_state)):
            is_invalid_types = True

        if not is_number_node(self.right_node.primitive_value(interpreter_state)):
            is_invalid_types = True

        if is_invalid_types:
            raise ValueError(
                error_templates.invalid_parameter.format(interpreter_state.cursor_row_number)
            )
