from bridge.interpreter.constants import ErrorMessages

from .mixins.binary_node import BinaryOperationNodeMixin
from ..choices import OperationNodeTypes
from ..interfaces import OperationErrorTemplates, OperationNode


class AndLogicNodeOperation(BinaryOperationNodeMixin, OperationNode):

    @property
    def type(self):
        return OperationNodeTypes.AND_LOGIC_NODE

    @property
    def value(self):
        raise NotImplementedError()

    def primitive_value(self, interpreter_state):
        return self.result

    def run(self, interpreter_state):
        error_templates = OperationErrorTemplates(
            invalid_operation=ErrorMessages.INVALID_LOGIC_OPERATION,
            invalid_parameter=ErrorMessages.INVALID_LOGIC_PARAMETERS,
        )
        self._run_binary_operation(interpreter_state,
                                   lambda left_value, right_value: left_value and right_value,
                                   error_templates)
