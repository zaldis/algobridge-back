from bridge.interpreter.interfaces.state import State
from bridge.interpreter.operation_composite.operation_nodes.empty import EmptyNodeOperation
from bridge.interpreter.compiler import VisualOperationCompiler
from bridge.interpreter.constants import ErrorMessages
from typing import Dict, Text

from ..choices import OperationNodeTypes
from ..interfaces import OperationNode
from ..services import is_assigned_variable_node


class ConditionNodeOperation(OperationNode):

    def __init__(self, body: OperationNode):
        self.__condition_body = body

    @classmethod
    def create_from_code(cls, operation_data: Dict) -> OperationNode:
        from ..operation_converter import convert_to_operation
        condition_body = convert_to_operation(operation_data['parameter'].get('param1'))
        return cls(condition_body)

    @property
    def body(self):
        return self.__condition_body

    @property
    def type(self):
        return OperationNodeTypes.CONDITION_NODE

    @property
    def value(self):
        raise NotImplementedError()

    def primitive_value(self, interpreter_state):
        return EmptyNodeOperation()

    def primitive_body(self, interpreter_state):
        return self.body.primitive_value(interpreter_state).value

    def run(self, interpreter_state: State):
        self.body.run(interpreter_state)

        self.__validate_operation(interpreter_state)

        if not self.primitive_body(interpreter_state):
            end_condition = interpreter_state.get_block_end(interpreter_state.cursor_row_number)
            self.__display_fail_condition(interpreter_state)
            interpreter_state.move_cursor_to(end_condition)
        self.__display_correct_condition(interpreter_state)

    def __display_fail_condition(self, interpreter_state):
        interpreter_state.add_visual_command(
            self.__create_base_condition(interpreter_state, '-')
        )

    def __display_correct_condition(self, interpreter_state):
        interpreter_state.add_visual_command(
            self.__create_base_condition(interpreter_state, '+')
        )

    def __create_base_condition(self, interpreter_state: State, condition_status: Text) -> Dict:
        return VisualOperationCompiler.compile_condition_operation(
            interpreter_state.cursor_row_number,
            str(self.primitive_body(interpreter_state)),
            condition_status
        )

    def __validate_operation(self, interpreter_state):
        ROW_NUMBER = interpreter_state.cursor_row_number
        if not self.body:
            raise ValueError(ErrorMessages.INVALID_CONDITION_PARAMETERS.format(ROW_NUMBER))

        end_condition = interpreter_state.get_block_end(ROW_NUMBER)
        if not end_condition:
            raise ValueError(
                ErrorMessages.CONDITION_WITHOUT_END.format(ROW_NUMBER)
            )

        if not is_assigned_variable_node(self.body, interpreter_state):
            raise ValueError(
                ErrorMessages.NON_INITIALIZE_VARIABLE.format(
                    ROW_NUMBER, self.body.value
                )
            )
        self.__validate_parameter_types(interpreter_state)

    def __validate_parameter_types(self, interpreter_state):
        is_invalid_types = False

        if self.body.primitive_value(interpreter_state).type != OperationNodeTypes.NUMBER_NODE:
            is_invalid_types = True

        if is_invalid_types:
            ROW_NUMBER = interpreter_state.cursor_row_number
            raise ValueError(
                ErrorMessages.INVALID_CONDITION_PARAMETERS.format(ROW_NUMBER)
            )
