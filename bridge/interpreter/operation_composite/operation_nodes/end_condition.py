from typing import Dict, List

from ..choices import OperationNodeTypes
from ..interfaces import OperationNode


class EndConditionNodeOperation(OperationNode):

    def __init__(self, *args, **kwargs):
        ...

    @classmethod
    def create_from_code(cls, operation_data: Dict) -> OperationNode:
        return cls()

    @property
    def type(self):
        return OperationNodeTypes.END_CONDITION_NODE

    @property
    def value(self):
        ...

    def primitive_value(self, interpreter_state):
        return self

    def run(self, interpreter_state):
        self.__make_display(
            interpreter_state.visual_commands,
            interpreter_state.cursor_row_number
        )

    def __make_display(self, visual_commands: List, cursor_row: int):
        visual_commands.append({
            'row': cursor_row,
            'type': 'end-condition'
        })

    def __validate_operation(self, cursor_manager):
        ...
