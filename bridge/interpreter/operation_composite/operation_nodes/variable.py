from bridge.interpreter.interfaces.state import State
from typing import Text

from bridge.interpreter.constants import ErrorMessages

from ..operation_nodes.empty import EmptyNodeOperation
from ..choices import OperationNodeTypes
from ..interfaces import OperationNode


class VariableNodeOperation(OperationNode):

    def __init__(self, name: Text):
        self.__name = name

    @classmethod
    def create_from_code(cls, operation_data):
        name = operation_data['parameter'].get('name')
        return cls(name)

    @property
    def type(self):
        return OperationNodeTypes.VARIABLE_NODE

    @property
    def value(self):
        return self.__name

    def primitive_value(self, interpreter_state):
        target_variable = interpreter_state[self.value]
        if not target_variable:
            return EmptyNodeOperation()
        return interpreter_state[self.value].saved_operation.primitive_value(interpreter_state)

    def run(self, interpreter_state: State):
        self.__validate_operation(interpreter_state.cursor_row_number)

    def __validate_operation(self, row_number):
        if not self.__name:
            raise ValueError(
                ErrorMessages.INVALID_VARIABLE.format(row_number)
            )
