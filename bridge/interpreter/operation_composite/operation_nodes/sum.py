from bridge.interpreter.interfaces.state import State
from bridge.interpreter.constants import ErrorMessages

from .mixins.binary_node import BinaryOperationNodeMixin
from ..choices import OperationNodeTypes
from ..interfaces import OperationErrorTemplates, OperationNode


class SumNodeOperation(BinaryOperationNodeMixin, OperationNode):

    @property
    def type(self):
        return OperationNodeTypes.SUM_NODE

    @property
    def value(self):
        raise NotImplementedError()

    def primitive_value(self, interpreter_state):
        return self.result

    def run(self, interpreter_state: State):
        error_templates = OperationErrorTemplates(
            invalid_operation=ErrorMessages.INVALID_SUM_OPERATION,
            invalid_parameter=ErrorMessages.INVALID_SUM_PARAMETERS,
        )
        self._run_binary_operation(interpreter_state,
                                   lambda left, right: left + right,
                                   error_templates)
