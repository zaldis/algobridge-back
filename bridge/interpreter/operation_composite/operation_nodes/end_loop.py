from bridge.interpreter.interfaces.state import State
from typing import Dict, List

from ..choices import OperationNodeTypes
from ..interfaces import OperationNode


class EndLoopNodeOperation(OperationNode):

    def __init__(self, *args, **kwargs):
        ...

    @classmethod
    def create_from_code(cls, operation_data: Dict) -> OperationNode:
        return cls()

    @property
    def type(self):
        return OperationNodeTypes.END_LOOP_NODE

    @property
    def value(self):
        ...

    def primitive_value(self, interpreter_state):
        return self

    def run(self, interpreter_state: State):
        ROW_NUMBER = interpreter_state.cursor_row_number
        start_block = interpreter_state.get_block_end(ROW_NUMBER)
        self.__make_display(ROW_NUMBER, interpreter_state.visual_commands)
        if start_block is not None:
            interpreter_state.move_cursor_to(start_block - 1)

    def __make_display(self, cursor_row: int, visual_commands: List):
        visual_commands.append({
            'type': 'end-for-loop',
            'row': cursor_row
        })


    def __validate_operation(self, cursor_manager):
        ...
