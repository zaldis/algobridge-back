from typing import Dict, List

from ..choices import OperationNodeTypes
from ..interfaces import OperationNode


class ArrayNodeOperation(OperationNode):

    def __init__(self, items: List, items_count: OperationNode):
        self.__items = items
        self.__items_count = items_count

    @classmethod
    def create_from_code(cls, operation_data: Dict) -> OperationNode:
        from ..operation_converter import convert_to_operation

        items_count = convert_to_operation(operation_data['parameter']['items_count'])
        items = list(operation_data['parameter'].get('items', []))
        return cls(items, items_count)

    @property
    def type(self):
        return OperationNodeTypes.ARRAY_NODE

    @property
    def value(self) -> List:
        return self.__items

    def primitive_value(self, interpreter_state):
        return self

    def run(self, interpreter_state):
        array_length = self.__items_count.value
        if not len(self.__items):
            self.__items = [0] * array_length
