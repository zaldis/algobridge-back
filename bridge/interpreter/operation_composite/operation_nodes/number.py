from typing import Dict
from ..choices import OperationNodeTypes
from ..interfaces import OperationNode


class NumberNodeOperation(OperationNode):

    def __init__(self, value: int):
        self.__value = value

    @classmethod
    def create_from_code(cls, operation_data: Dict) -> OperationNode:
        value = int(operation_data['parameter']['val'])
        return cls(value)

    @property
    def type(self):
        return OperationNodeTypes.NUMBER_NODE

    @property
    def value(self) -> int:
        return self.__value

    def primitive_value(self, interpreter_state):
        return self

    def run(self, interpreter_state):
        ...
