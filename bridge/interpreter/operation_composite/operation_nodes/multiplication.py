from bridge.interpreter.constants import ErrorMessages

from .mixins.binary_node import BinaryOperationNodeMixin
from ..choices import OperationNodeTypes
from ..interfaces import OperationErrorTemplates, OperationNode


class MultiplicationNodeOperation(BinaryOperationNodeMixin, OperationNode):

    @property
    def type(self):
        return OperationNodeTypes.MULTIPLICATION_NODE

    @property
    def value(self):
        raise NotImplementedError()

    def primitive_value(self, interpreter_state):
        return self.result

    def run(self, interpreter_state):
        error_templates = OperationErrorTemplates(
            invalid_operation=ErrorMessages.INVALID_MULTIPLICATION_OPERATION,
            invalid_parameter=ErrorMessages.INVALID_MULTIPLICATION_PARAMETERS,
        )
        self._run_binary_operation(interpreter_state,
                                   lambda left, right: left * right,
                                   error_templates)
