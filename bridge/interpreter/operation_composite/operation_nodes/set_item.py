from bridge.interpreter.interfaces.state import State
from bridge.interpreter.operation_composite.operation_nodes.variable import VariableNodeOperation
from bridge.interpreter.operation_composite.operation_nodes.assign import AssignNodeOperation
from bridge.interpreter.operation_composite.operation_nodes.array import ArrayNodeOperation
from bridge.interpreter.operation_composite.operation_nodes.number import NumberNodeOperation
from bridge.interpreter.constants import ErrorMessages
from typing import Dict, Text

from ..choices import OperationNodeTypes
from ..interfaces import OperationNode
from ..services import is_assigned_variable_node


class SetItemNodeOperation(OperationNode):

    def __init__(self, arr_name: Text, index: OperationNode, value: OperationNode):
        self.__arr_name = arr_name
        self.__index = index
        self.__new_value = value

    @classmethod
    def create_from_code(cls, operation_data: Dict) -> OperationNode:
        from ..operation_converter import convert_to_operation
        arr_name = str(convert_to_operation(operation_data['parameter'].get('arrName')).value)
        index = convert_to_operation(operation_data['parameter'].get('index'))
        value = convert_to_operation(operation_data['parameter'].get('newValue'))
        return cls(arr_name, index, value)

    @property
    def arr_name(self):
        return self.__arr_name

    @property
    def index(self):
        return self.__index

    @property
    def new_value(self):
        return self.__new_value

    @property
    def type(self):
        return OperationNodeTypes.SET_ITEM_NODE

    @property
    def value(self):
        raise NotImplementedError()

    def primitive_value(self, interpreter_state):
        return self

    def run(self, interpreter_state: State):
        self.index.run(interpreter_state)
        self.new_value.run(interpreter_state)

        self.__validate_operation(interpreter_state)

        array = interpreter_state[self.arr_name].saved_operation.primitive_value(interpreter_state).value
        index = self.index.primitive_value(interpreter_state).value
        new_value = self.new_value.primitive_value(interpreter_state).value

        array[index] = new_value
        assign_node = AssignNodeOperation(
            VariableNodeOperation(name=self.arr_name),
            ArrayNodeOperation(array, NumberNodeOperation(len(array)))
        )
        assign_node.run(interpreter_state)

    def __validate_operation(self, interpreter_state: State):
        ROW_NUMBER = interpreter_state.cursor_row_number
        if not (self.arr_name and self.index and self.new_value):
            raise ValueError(ErrorMessages.INVALID_SET_BY_INDEX_FUNCTION.format(ROW_NUMBER))

        if not is_assigned_variable_node(self.index, interpreter_state):
            raise ValueError(
                ErrorMessages.NON_INITIALIZE_VARIABLE.format(
                    ROW_NUMBER, self.index.value
                )
            )
        if not is_assigned_variable_node(self.new_value, interpreter_state):
            raise ValueError(
                ErrorMessages.NON_INITIALIZE_VARIABLE.format(
                    ROW_NUMBER, self.index.value
                )
            )
        self.__validate_parameter_types(interpreter_state)

    def __validate_parameter_types(self, interpreter_state: State):
        ROW_NUMBER = interpreter_state.cursor_row_number
        is_invalid_types = False

        if self.index.primitive_value(interpreter_state).type != OperationNodeTypes.NUMBER_NODE:
            is_invalid_types = True

        if self.new_value.primitive_value(interpreter_state).type != OperationNodeTypes.NUMBER_NODE:
            is_invalid_types = True

        array = interpreter_state[self.arr_name].saved_operation.primitive_value(interpreter_state).value
        index = self.index.primitive_value(interpreter_state).value
        if len(array) < index or index < 0:
            is_invalid_types = True

        if is_invalid_types:
            raise ValueError(
                ErrorMessages.INVALID_GET_BY_INDEX_FUNCTION.format(ROW_NUMBER)
            )
