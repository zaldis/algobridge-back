from typing import Dict
from ..choices import OperationNodeTypes
from ..interfaces import OperationNode


class EmptyNodeOperation(OperationNode):

    def __init__(self, *args, **kwargs):
        ...

    @classmethod
    def create_from_code(cls, operation_data: Dict) -> OperationNode:
        return cls()

    @property
    def type(self):
        return OperationNodeTypes.EMPTY_NODE

    @property
    def is_empty(self) -> bool:
        return True

    @property
    def value(self):
        return ''

    def primitive_value(self, interpreter_state):
        return self

    def run(self, interpreter_state):
        ...

    def __validate_operation(self, cursor_manager):
        ...
