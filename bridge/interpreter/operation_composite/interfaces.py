from __future__ import annotations

from abc import ABCMeta, abstractmethod
from bridge.interpreter.constants import ErrorMessages
from typing import Dict, List, NamedTuple, Text, Union
from bridge.interpreter.interfaces.state import State


class OperationNode(metaclass=ABCMeta):

    @classmethod
    def create_from_code(cls, operation_data: Dict) -> OperationNode:
        ...

    @property
    def is_empty(self) -> bool:
        return False

    @property
    @abstractmethod
    def type(self):
        ...

    @property
    @abstractmethod
    def value(self) -> Union[List, Text, int, float]:
        ...

    @abstractmethod
    def primitive_value(self, interpreter_state: State) -> OperationNode:
        ...

    @abstractmethod
    def run(self,
            interpreter_state: State):
        ...


class OperationErrorTemplates(NamedTuple):
    invalid_operation: Text = ''
    invalid_parameter: Text = ''
    non_initialize: Text = ErrorMessages.NON_INITIALIZE_VARIABLE
