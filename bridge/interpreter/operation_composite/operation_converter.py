from bridge.interpreter.constants import ErrorMessages
from typing import Dict

from .operation_nodes.array import ArrayNodeOperation
from .operation_nodes.assign import AssignNodeOperation
from .operation_nodes.empty import EmptyNodeOperation
from .operation_nodes.number import NumberNodeOperation
from .operation_nodes.variable import VariableNodeOperation
from .operation_nodes.sum import SumNodeOperation
from .operation_nodes.subtraction import SubtractionNodeOperation
from .operation_nodes.multiplication import MultiplicationNodeOperation
from .operation_nodes.division import DivisionNodeOperation
from .operation_nodes.larger import LargerNodeOperation
from .operation_nodes.larger_equal import LargerEqualNodeOperation
from .operation_nodes.less import LessNodeOperation
from .operation_nodes.less_equal import LessEqualNodeOperation
from .operation_nodes.equal import EqualNodeOperation
from .operation_nodes.and_logic import AndLogicNodeOperation
from .operation_nodes.or_logic import OrLogicNodeOperation
from .operation_nodes.condition import ConditionNodeOperation
from .operation_nodes.end_condition import EndConditionNodeOperation
from .operation_nodes.loop import LoopNodeOperation
from .operation_nodes.end_loop import EndLoopNodeOperation
from .operation_nodes.get_item import GetItemNodeOperation
from .operation_nodes.set_item import SetItemNodeOperation
from .interfaces import OperationNode


def convert_to_operation(operation_data: Dict) -> OperationNode:
    operation_type = operation_data['type']

    operation_converters = {
        'assign': AssignNodeOperation,
        'variable': VariableNodeOperation,
        'array': ArrayNodeOperation,
        'number': NumberNodeOperation,
        'empty': EmptyNodeOperation,
        'sum': SumNodeOperation,
        'subtraction': SubtractionNodeOperation,
        'multiplication': MultiplicationNodeOperation,
        'division': DivisionNodeOperation,
        'larger': LargerNodeOperation,
        'larger-equal': LargerEqualNodeOperation,
        'less': LessNodeOperation,
        'less-equal': LessEqualNodeOperation,
        'equal': EqualNodeOperation,
        'and-logic': AndLogicNodeOperation,
        'or-logic': OrLogicNodeOperation,
        'condition': ConditionNodeOperation,
        'end-condition': EndConditionNodeOperation,
        'for-loop': LoopNodeOperation,
        'end-for-loop': EndLoopNodeOperation,
        'get-item': GetItemNodeOperation,
        'set-item': SetItemNodeOperation,
    }
    if operation_type not in operation_converters:
        raise ValueError(f'Invalid operation type: {operation_type}')
    return operation_converters[operation_type].create_from_code(operation_data)
