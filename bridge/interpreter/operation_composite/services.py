from bridge.interpreter.state import State
from bridge.interpreter.operation_composite.choices import OperationNodeTypes
from typing import Text, Tuple
from .interfaces import OperationNode


def validate_node_type(node: OperationNode, available_types: Tuple, error_massage: Text):
    if not isinstance(node, available_types):
        raise ValueError(error_massage)


def is_primitive_node(node: OperationNode) -> bool:
    return node.type in [
        OperationNodeTypes.NUMBER_NODE,
        OperationNodeTypes.ARRAY_NODE
    ]


def is_assigned_variable_node(node: OperationNode, interpreter_state: State) -> bool:
    if node.type == OperationNodeTypes.VARIABLE_NODE:
        if not interpreter_state[str(node.value)]:
            return False
    return True


def is_number_node(node: OperationNode) -> bool:
    return node.type == OperationNodeTypes.NUMBER_NODE
