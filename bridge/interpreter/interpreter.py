from bridge.interpreter.constants import ErrorMessages
from typing import List
from bridge.interpreter.operation_composite.choices import OperationNodeTypes
from bridge.interpreter.operation_composite.interfaces import OperationNode
from bridge.interpreter.operation_composite.operation_converter import convert_to_operation
from copy import deepcopy

from . import state
from bridge.interpreter.interfaces.state import State


__all__ = ['Interpreter']


class Interpreter(object):

    __slots__ = (
        'state', 'operations', 'operations_count', 'cursor_manager',
        'operation_nodes'
    )

    def __init__(self, operations):
        self.operations = deepcopy(operations)
        self.operations_count = len(operations)
        self.state: State = state.StateImp(self.operations_count)

        self.__init_operation_nodes()

    @property
    def visual_operations(self):
        return self.state.visual_commands

    def __init_operation_nodes(self):
        self.operation_nodes: List[OperationNode] = []
        for row_number, operation_data in enumerate(self.operations):
            self.operation_nodes.append(self.__make_operation_node(operation_data, row_number))

        self.__init_condition_ranges()
        self.__init_loop_ranges()

    def __make_operation_node(self, operation_data, row_number):
        try:
            operation_node = convert_to_operation(operation_data)
        except ValueError:
            operation_type = operation_data.get('type')
            raise ValueError(ErrorMessages.INVALID_OPERATION_TYPE.format(row_number, operation_type))
        return operation_node

    def __init_condition_ranges(self):
        self.__init_block_ranges(OperationNodeTypes.CONDITION_NODE,
                                 OperationNodeTypes.END_CONDITION_NODE)

    def __init_loop_ranges(self):
        self.__init_block_ranges(OperationNodeTypes.LOOP_NODE,
                                 OperationNodeTypes.END_LOOP_NODE)

    def __init_block_ranges(self, start_block, end_block):
        stacked_rows = []

        for row in range(0, self.operations_count):
            operation_node = self.operation_nodes[row]

            if operation_node.type == start_block:
                stacked_rows.append(row)
            if operation_node.type == end_block:
                if len(stacked_rows):
                    start_condition = stacked_rows.pop()
                    end_condition = row
                    self.state.add_block_range(start_condition, end_condition)

    def implement(self):
        while not self.state.is_cursor_finished:
            target_operation = self.operation_nodes[self.state.cursor_row_number]
            if not target_operation.is_empty:
                target_operation.run(self.state)
            self.state.move_cursor_next()
