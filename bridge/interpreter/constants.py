class ErrorMessages(object):
    INVALID_OPERATION = "row:{} | All operations must have 'type' and 'parameter' fields"
    INVALID_OPERATION_TYPE = "row:{} | Server doesn't handle the {} operation"
    INVALID_VARIABLE = "row:{} | Variable operation must have 'name' field"
    INVALID_NUMBER = "row:{} | Number operation must have 'val' field"
    INVALID_ASSIGN = "row:{} | Assign operation must have 'left' and 'right' fields"
    INVALID_ASSIGN_PARAMETERS = "row:{} | Assign operation expect primitive or variable operation as right child and variable as left child"
    INVALID_SUM_OPERATION = "row:{} | Sum operation must have 'left' and 'right' fields"
    INVALID_SUM_PARAMETERS = "row:{} | Sum operation expects numbers and variable operation with number type"
    INVALID_SUBTRACTION_OPERATION = "row:{} | Subtraction operation must have 'left' and 'right' fields"
    INVALID_SUBTRACTION_PARAMETERS = "row:{} | Subtraction operation expects numbers and variable operation with number type"
    INVALID_MULTIPLICATION_OPERATION = "row:{} | Multiplication operation must have 'left' and 'right' fields"
    INVALID_MULTIPLICATION_PARAMETERS = "row:{} | Multiplication operation expects numbers and variable operation with number type"
    INVALID_DIVISION_OPERATION = "row:{} | Division operation must have 'left' and 'right' fields"
    INVALID_DIVISION_PARAMETERS = "row:{} | Division operation expects numbers and variable operation with number type"
    INVALID_COMPARION_OPERATION = "row:{} | Comparison operation must have 'left' and 'right' fields"
    INVALID_COMPARION_PARAMETERS = "row:{} | Comparison operation expects numbers and variable operation with number type"
    INVALID_LOGIC_OPERATION = "row:{} | Logic operation must have 'left' and 'right' fields"
    INVALID_LOGIC_PARAMETERS = "row:{} | Logic operation expects numbers and variable operation with number type"

    CONDITION_WITHOUT_END = "row:{} | Condition operation without end"
    INVALID_CONDITION_PARAMETERS = "row:{} | Condition operation expects numbers and variable operation with number type"

    INVALID_FOR_LOOP_PARAMETERS = "row:{} | For loop operation expects variable as index parameter and number type or variable with number type as start, end, step paremeters"
    FOR_LOOP_WITHOUT_END = "row:{} | For loop operation without end"
    INFINITE_FOR_LOOP = "row:{} | It looks like infinite loop. Max limit is {} iterations"

    NON_INITIALIZE_VARIABLE = "row:{} | You must initialize `{}` variable before use it."

    INVALID_FUNCTION = "row:{} | Function operation expects name parameter"

    INVALID_ARRAY_OPERATION = "row:{} | Array operation expects items_count parameter with number type"
    INVALID_SET_BY_INDEX_FUNCTION = "row:{} | Set by index function expects array-based variable param, index (number type) param and value (number type) param"
    INVALID_GET_BY_INDEX_FUNCTION = "row:{} | Get by index function expects array-based variable param and index (number type) param"

    INDEX_OUT_OF_RANGE = "row:{} | Index out of range"
    ZERO_DIVISION_ERROR = "row:{} | Division by zero"


class Limits(object):
    MAX_LOOP_COUNT = 50
