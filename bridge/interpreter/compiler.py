class OperationCompiler(object):

    @staticmethod
    def compile_assign_operation(variable_name: str, operation: dict) -> dict:
        return {
            'type': 'assign',
            'parameter': {
                'left': OperationCompiler.compile_variable_operation(variable_name),
                'right': operation,
            }
        }

    @staticmethod
    def compile_variable_operation(variable_name: str) -> dict:
        return {
            'type': 'variable',
            'parameter': {
                'name': variable_name
            }
        }

    @staticmethod
    def compile_number_operation(value) -> dict:
        return {
            'type': 'number',
            'parameter': {
                'val': value
            }
        }

    @staticmethod
    def compile_array_operation(array: list) -> dict:
        return {
            'type': 'array',
            'parameter': {
                'items_count': OperationCompiler.compile_number_operation(len(array)),
                'items': array
            }
        }


class VisualOperationCompiler(object):

    @staticmethod
    def compile_simple_assign_operation(
            cursor_row: int, target_name: str, old_value: str, new_value: str) -> dict:
        return {
            'row': cursor_row,
            'type': 'simple_assign',
            'name': target_name,
            'oldValue': old_value,
            'newValue': new_value
        }

    @staticmethod
    def compile_from_variable_assign_operation(
            cursor_row: int, target_name: str, target_value: str, source_name: str,
            source_value: str) -> dict:
        return {
            'row': cursor_row,
            'type': 'assign_from_variable',
            'nameA': target_name,
            'valueA': target_value,
            'nameB': source_name,
            'valueB': source_value
        }

    @staticmethod
    def compile_condition_operation(cursor_row: int, target_value: str, is_correct: str='-') -> dict:
        return {
            'row': cursor_row,
            'type': 'condition',
            'value': target_value,
            'isCorrect': is_correct
        }
