from typing import Dict, List, Text

from bridge.interpreter.interfaces.handler import HandlerInfoInterface, ParameterInterface


ADDITION_CORE_HANDLER = 'addition'
SUBTRACTION_CORE_HANDLER = 'subtraction'
MULTIPLICATION_CORE_HANDLER = 'multiplication'
DIVISION_CORE_HANDLER = 'division'

CORE_HANDLERS = (
    (ADDITION_CORE_HANDLER, 'Addition handler'),
    (SUBTRACTION_CORE_HANDLER, 'Subtraction handler'),
    (MULTIPLICATION_CORE_HANDLER, 'Multiplication handler'),
    (DIVISION_CORE_HANDLER, 'Division handler')
)


BINARY_PARAMS: List[ParameterInterface] = [
    ParameterInterface(
        display = 'left',
        description = 'Left operand',
        order = 1,
    ),
    ParameterInterface(
        display = 'right',
        description = 'Right operand',
        order = 2,
    ),
]


"""
    Structure that contains base information about operation handlers.
"""
CORE_HANDLE_INFOS: Dict[Text, HandlerInfoInterface] = {
    ADDITION_CORE_HANDLER: HandlerInfoInterface(
        params = BINARY_PARAMS,
        description = 'left + right'
    ),

    SUBTRACTION_CORE_HANDLER: HandlerInfoInterface(
        params = BINARY_PARAMS,
        description = 'left - right'
    ),

    MULTIPLICATION_CORE_HANDLER: HandlerInfoInterface(
        params = BINARY_PARAMS,
        description = 'left * right'
    ),

    DIVISION_CORE_HANDLER: HandlerInfoInterface(
        params = BINARY_PARAMS,
        description = 'left / right'
    )
}
