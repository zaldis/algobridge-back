from abc import ABCMeta
from typing import Optional, Text

from .state_mixins import (
    CursorManagerMixin, ForLoopsContainerMixin, LogicBlocksMixin,
    VariablesContainerMixin, VisualCommandsContainerMixin
)
from ..data_structures.variable import Variable


class State(CursorManagerMixin,
            ForLoopsContainerMixin,
            LogicBlocksMixin,
            VariablesContainerMixin,
            VisualCommandsContainerMixin,
            metaclass=ABCMeta):

    def __getitem__(self, variable_name: Text) -> Optional[Variable]:
        return self.get_variable(variable_name)

    def __setitem__(self, variable_name: Text, operation) -> None:
        self.add_variable(variable_name, operation)
