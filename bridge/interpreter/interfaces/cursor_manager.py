from abc import ABCMeta, abstractmethod


class CursorManager(metaclass=ABCMeta):

    def __init__(self, cursor_row: int = 0, operations_count: int = 0):
        self.__cursor_row = cursor_row
        self.__operations_count = operations_count

    @property
    def cursor_row(self):
        return self.__cursor_row

    @cursor_row.setter
    def cursor_row(self, new_cursor_row: int):
        if new_cursor_row <= self.operations_count + 1:
            self.__cursor_row = new_cursor_row
        else:
            raise AttributeError('Cursor row can not be outer the operations count')

    @property
    def operations_count(self):
        return self.__operations_count

    @property
    @abstractmethod
    def is_finished(self) -> bool:
        ...

    @abstractmethod
    def move_next(self) -> None:
        ...
