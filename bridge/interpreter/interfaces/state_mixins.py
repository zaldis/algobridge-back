import copy

from collections import defaultdict
from typing import Dict, List, Optional, Text

from bridge.interpreter.cursor_manager import CursorManagerImp

from .cursor_manager import CursorManager
from ..data_structures.variable import Variable


class VariablesContainerMixin(object):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__saved_variables: Dict[Text, Variable] = {}

    @property
    def saved_variables(self):
        return self.__saved_variables

    def add_variable(self, name, saved_operation):
        operation_copy = copy.deepcopy(saved_operation)

        variable = Variable(name, operation_copy)
        self.saved_variables[name] = variable

    def get_variable(self, name: str) -> Optional[Variable]:
        result_variable = self.saved_variables.get(name, None)
        if result_variable:
            return copy.deepcopy(result_variable)
        return None


class ForLoopsContainerMixin(object):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__worked_for_loops = defaultdict(int)

    def up_loop_iterations(self, loop_id):
        self.__worked_for_loops[loop_id] += 1

    def reset_loop_iterations(self, loop_id):
        self.__worked_for_loops[loop_id] = 0

    def get_loop_iterations(self, loop_id):
        return self.__worked_for_loops[loop_id]


class CursorManagerMixin(object):
    def __init__(self, rows_count: int, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__cursor_manage: CursorManager = CursorManagerImp(0, rows_count)

    @property
    def cursor_row_number(self) -> int:
        return self.__cursor_manage.cursor_row

    @property
    def is_cursor_finished(self) -> bool:
        return self.__cursor_manage.is_finished

    def move_cursor_next(self):
        self.__cursor_manage.move_next()

    def move_cursor_to(self, row_number: int):
        self.__cursor_manage.cursor_row = row_number


class LogicBlocksMixin(object):
    def __init__(self, *args, **kwargs):
        self.__block_ranges = {}
        super().__init__(*args, **kwargs)

    def add_block_range(self, start: int, end: int):
        self.__block_ranges[start] = end
        self.__block_ranges[end] = start

    def get_block_end(self, start: int) -> Optional[int]:
        return self.__block_ranges.get(start, None)


class VisualCommandsContainerMixin(object):
    def __init__(self, *args, **kwargs):
        self.__visual_commands = []
        super().__init__(*args, **kwargs)

    @property
    def visual_commands(self) -> List:
        return self.__visual_commands

    def add_visual_command(self, visual_command):
        self.__visual_commands.append(visual_command)
