from typing import List, NamedTuple, Text


class ParameterInterface(NamedTuple):
    """
    Structure that contains base information about required params.
        - display:
            How that parameter will be displayed for users.
        - description:
            Describe parameter logic
        - order:
            Identification to put parameter into operation pattern
    """
    display: Text
    description: Text
    order: int


class HandlerInfoInterface(NamedTuple):
    params: List[ParameterInterface]
    description: Text


