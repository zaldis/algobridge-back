from dataclasses import dataclass
from typing import Any, Text


@dataclass
class Variable(object):
    __slots__ = ['name', 'saved_operation']

    name: Text
    saved_operation: Any
