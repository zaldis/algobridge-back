from .interfaces.cursor_manager import CursorManager


class CursorManagerImp(CursorManager):

    @property
    def is_finished(self) -> bool:
        return self.cursor_row >= self.operations_count

    def move_next(self, rows_count: int = 1) -> None:
        self.cursor_row += rows_count
