from bridge.choices import ASSIGN_OPERATION, OPERATION_TYPES
from django.contrib.auth.models import User
from django.db import models


class Algorithm(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    description = models.TextField()
    implementation = models.TextField()

    def __str__(self):
        return f"{self.author.username}::{self.title}"


class OperationCategory(models.Model):
    name = models.CharField(max_length=100, unique=True, default='name')
    display_name = models.CharField(max_length=100, unique=True, default='Name')

    def __str__(self):
        return self.display_name


class OperationType(models.Model):
    name = models.CharField(max_length=100, unique=True, choices=OPERATION_TYPES, default=ASSIGN_OPERATION)
    display_name = models.CharField(max_length=100, unique=True, default='Operation type')
    is_primitive = models.BooleanField()
    parameters = models.TextField(default='', blank=True, help_text='Comma + new line separated')
    category = models.ForeignKey(OperationCategory, on_delete=models.CASCADE, null=True, blank=True)
    enabled = models.BooleanField(default=True)

    def __str__(self):
        return f"{self.display_name}: {self.parameters}"


# class Operation(models.Model):
#     title = models.CharField(max_length=200, help_text='Keyword in code')
#     display_title = models.CharField(max_length=200, help_text='Display for users')
#     description = models.TextField()
#     category = models.CharField(max_length=100, choices=OPERATION_CATEGORY_CHOICES)
#     core_handler = models.CharField(max_length=100, choices=CORE_HANDLERS, null=True, blank=True)
#     author = models.ForeignKey(User, on_delete=models.CASCADE)
#     hidden = models.BooleanField(default=False)
#
#     class Meta:
#         unique_together = ('title', 'author', )
#
#     def __str__(self):
#         return f"{self.title}::{self.author.username}"
#
#     def clean(self, *args, **kwargs):
#         if not self.core_handler:
#             raise ValidationError(
#                 f'You have to select handler for the operation'
#             )
#
#     @staticmethod
#     def validate_operation_display(handler: Text, param_values: List[Text]) -> None:
#         """
#             Check that all necessary operation params was set in the correct way
#         """
#         handler_info = CORE_HANDLE_INFOS[handler]
#         param_displays = [param.display for param in handler_info.params]
#         for param_value in param_values:
#             if param_value not in param_displays:
#                 raise ValidationError(
#                     f'Operation param with display={param_value}, does not exist'
#                 )
#
#         if set(param_displays) != set(param_values):
#             params_info = ''
#             for parameter in handler_info.params:
#                 order = parameter.order
#                 description = parameter.description
#                 params_info += f'{order}: {parameter.display} | {description}; '
#
#             raise ValidationError(
#                 f'Operation has to display params with next names: {param_displays}'
#                 f' ({params_info})'
#             )
#
#
# class OperationToken(models.Model):
#     order = models.IntegerField()
#     operation = models.ForeignKey(Operation, on_delete=models.CASCADE)
#     type = models.CharField(choices=OPERATION_TOKEN_TYPE, max_length=30)
#     value = models.CharField(max_length=100, blank=True)
#
#     class Meta:
#         ordering = ('order', )
#
#     def __str__(self):
#         return f'{self.operation.title} ({self.order})'
#
#     @staticmethod
#     def validate_orders(orders: List[int]):
#         """
#             Orders must not be dublicated
#         """
#         if len(orders) != len(set(orders)):
#             raise ValidationError('Orders must be unique values!')
