from django.contrib import admin

from .models import Algorithm, OperationCategory, OperationType


@admin.register(Algorithm)
class AlgorithmAdmin(admin.ModelAdmin):
    list_display = ('title', 'author')


@admin.register(OperationType)
class OperationType(admin.ModelAdmin):
    list_display = ('display_name', 'is_primitive', 'parameters', 'category', 'enabled')

    def get_readonly_fields(self, request, operation_type):
        if operation_type:
            # editing operation type that already exist
            return ('parameters', )
        return []


@admin.register(OperationCategory)
class OperationCategory(admin.ModelAdmin):
    list_display = ('display_name', )


# class OperationTokenInline(admin.TabularInline):
#     model = OperationToken
#     extra = 1
#
#
# class OperationForm(forms.ModelForm):
#     class Meta:
#         model = Operation
#         fields = '__all__'
#
#     def clean(self):
#         operation_token_keys = [key for key in self.data.keys() if 'operationtoken' in key]
#         value_keys = [key for key in operation_token_keys if 'value' in key]
#         type_keys = [key for key in operation_token_keys if 'type' in key]
#         operation_token_values = [self.data[key] for key in value_keys]
#         operation_token_types = [self.data[key] for key in type_keys]
#
#         token_param_values = []
#         for index, token_type in enumerate(operation_token_types):
#             if 'param' in token_type:
#                 token_param_values.append(operation_token_values[index])
#
#         Operation.validate_operation_display(self.data['core_handler'],
#                                              token_param_values)
#
#         order_keys = [key for key in operation_token_keys if 'order' in key]
#         orders = [int(self.data[key]) for key in order_keys if self.data[key]]
#         OperationToken.validate_orders(orders)
#
#
# @admin.register(Operation)
# class OperationAdmin(admin.ModelAdmin):
#     form = OperationForm
#
#     list_display = ('display_title', 'author', 'category')
#     list_filter = ('category', )
#     inlines = [OperationTokenInline]
#
