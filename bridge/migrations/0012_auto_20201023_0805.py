# Generated by Django 3.0.2 on 2020-10-23 08:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bridge', '0011_remove_operationd_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='operationtype',
            name='parameters',
            field=models.TextField(blank=True, default='', help_text='Comma + new line separated'),
        ),
    ]
