# Generated by Django 3.0.2 on 2020-10-22 21:52

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bridge', '0006_auto_20201022_2128'),
    ]

    operations = [
        migrations.AddField(
            model_name='operationtype',
            name='parameters',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='bridge.OperationType'),
        ),
    ]
