# Generated by Django 3.0.2 on 2020-07-15 13:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bridge', '0002_operation_operationparameter'),
    ]

    operations = [
        migrations.AddField(
            model_name='operation',
            name='core_handler',
            field=models.CharField(blank=True, choices=[('addition', 'Addition handler'), ('subtraction', 'Subtraction handler'), ('multiplication', 'Multiplication handler'), ('division', 'Division handler')], max_length=100, null=True),
        ),
    ]
