import os


def setup_setting_module():
    """
        Setup django setting module related to system user.

        Set setting file by pattern:
            algobridge.settings._{user_name}
    """
    if os.getenv('USER'):
        os.environ.setdefault('DJANGO_SETTINGS_MODULE', f'algobridge.settings._{os.environ["USER"]}')
    else:
        os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'algobridge.settings.default')
