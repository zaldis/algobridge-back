# AlgoBridge-back

Back-end side for AlgoBridge system

**HOW TO INSTALL:**

1. Create and activate or activate already created virtual environment.
1. Install requirements:
    * If using release mode:
        * Run command in the project root directory:
            `pip install -r requirements.txt`
    * If using debug mode:
        * Run commands (Fedora OS):
            * `sudo dnf install graphviz-devel`
            * `sudo dnf install python3-devel`
            * `pip install -r requirements-dev.txt`
1. Run command: `python manage.py migrate`.
1. If you have error: *django.db.utils.OperationalError: no such table: auth_user*:
    * Comment urls in the project.
    * Run the command again.
    * Remove created comments

**HOW TO RUN:**

1. Clone this repository
2. Install docker and run docker daemon (systemctl start docker)
3. Run command: `docker-compose up` (from root directory)
4. Run command: `./manage.py runserver`

**HOW TO DEPLOY:**

* To Elastic Beanstalk:
    1. Run command: `./manage.py collectstatic`
    2. Install eb console
    3. Initialize eb application and environment
    4. Run command: eb deploy

**HOW TO TEST:**

1. Sync dev requirements
2. Run `pytest --ds=algobridge.settings.<your_settings_file>`
